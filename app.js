const main = document.querySelector('main');

window.addEventListener('load', e => {
    retrieveData();
});

async function retrieveData() {
    const res = await fetch('https://jsonplaceholder.typicode.com/todos');
    const todos = await res.json();

    main.innerHTML = todos.map(createTodoCard).join('\n');
    console.log(todos);
}

function createTodoCard(todo) {
    return `
        <div class="card">
            <h2>${todo.title}</h2>
        </div>
    `;
}